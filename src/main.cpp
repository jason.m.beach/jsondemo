#include "nlohmann/json.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
#include <chrono>

// for convenience
using json = nlohmann::json;

struct Point{
  Point() = default;
  Point(double x, double y, uint64_t ts):x(x),y(y),timestamp(ts) {}
  double x = 0;
  double y = 0;
  uint64_t timestamp = 0;
};

void to_json(json& j, const Point& p) {
  //j = json{{"x", p.x}, {"y", p.y}, {"timestamp",p.timestamp}}; //key value pairs
  j["point"] = json{p.x, p.y, p.timestamp}; // typesafe json array
  //j = json{p.x, p.y, p.timestamp}; //json array (min size)
}

void from_json(const json& j, Point& p) {
  //j.at("x").get_to(p.x);
  //j.at("y").get_to(p.y);
  //j.at("timestamp").get_to(p.timestamp);
  j.at("point").at(0).get_to(p.x);
  j.at("point").at(1).get_to(p.y);
  j.at("point").at(2).get_to(p.timestamp);
  //j.at(0).get_to(p.x);
  //j.at(1).get_to(p.y);
  //j.at(2).get_to(p.timestamp);
}

typedef std::vector<Point> Polygon;

std::string to_string(const Polygon& poly)
{
  std::ostringstream oss;
  oss.precision(15);
  //oss << std::fixed << std::setprecision(6);
  for(auto& point : poly)
  {
    oss << " x: " << std::setw(20) << point.x << " y: " << std::setw(15) << point.y << " timestamp: " << point.timestamp << "\n";
  }
  return oss.str();
}

int main()
{
  Polygon poly;
  poly.emplace_back(2.3453,  42134563.2346236, 1542513176928201);
  poly.emplace_back(1223432.34523455,  1423833.22112346, 1542513176837594);
  poly.emplace_back(12340982550.3455,  74213.2312342146, 1542513176829374);
  poly.emplace_back(0.000345123561234456, 46323.2352112346, 1542513171872343);

  std::cout << "size of point: " << sizeof(Point) << " bytes.  size of polygon data: "
            << sizeof(Point) * poly.size() << std::endl;

  std::cout << "original poly: \n" << to_string(poly) << std::endl; //print the Polygon

  json json_poly;
  auto start = std::chrono::high_resolution_clock::now();
  json_poly["polylist"] = poly; //convert from Polygon (vector<Points>) to json requiring only to_json function above
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  std::cout << "conversion to json took: " << duration.count() << "us\n";
  std::cout << "json_poly dump: \n" << json_poly << "\n\n"; //print the json object

  std::vector<std::uint8_t> v_msgpack = json::to_msgpack(json_poly); //convert the json object to a msgpack byte stream
  std::vector<std::uint8_t> v_ubjson = json::to_ubjson(json_poly); //convert the json object to a universal binary json byte stream


  std::cout << "packed msgpack size: " << v_msgpack.size() << " bytes. packed ubjson size: " << v_ubjson.size() << "\n\n";

  // return trip
  json json_from_msgpack = json::from_msgpack(v_msgpack); //convert the msgpack byte stream back to a json object

  std::cout << "recovered json\n" << json_from_msgpack << std::endl;

  start = std::chrono::high_resolution_clock::now();
  Polygon poly_return = json_from_msgpack["polylist"]; //convert from json back to a Polygon using only from_json function above
  stop = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  std::cout << "conversion from json took: " << duration.count() << "us\n";


  std::cout << "recovered poly: \n" << to_string(poly_return) << std::endl; //print the Polygon

  return 0;
}
